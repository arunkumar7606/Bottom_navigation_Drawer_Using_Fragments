package com.arun.aashu.bottomnavigationbar.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.widget.TextView;

import com.arun.aashu.bottomnavigationbar.fragment.BlankFragment;
import com.arun.aashu.bottomnavigationbar.R;
import com.arun.aashu.bottomnavigationbar.fragment.SecondFragment;
import com.arun.aashu.bottomnavigationbar.fragment.ThirdFragment;

public class Bottom_Nav extends FragmentActivity {

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_home:

                    FragmentManager fm=getSupportFragmentManager();
                    FragmentTransaction ft=fm.beginTransaction();
                    ft.replace(R.id.frameId,new BlankFragment());
                    ft.commit();
                   break;

                case R.id.navigation_dashboard:

                    FragmentManager fm2=getSupportFragmentManager();
                    FragmentTransaction ft2=fm2.beginTransaction();
                    ft2.replace(R.id.frameId,new SecondFragment());
                    ft2.commit();
                    break;

                case R.id.navigation_notifications:

                    FragmentManager fm3=getSupportFragmentManager();
                    FragmentTransaction ft3=fm3.beginTransaction();
                    ft3.replace(R.id.frameId,new ThirdFragment());
                    ft3.commit();
                    break;

            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_bottom);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        FragmentManager fm=getSupportFragmentManager();
        FragmentTransaction ft=fm.beginTransaction();
        ft.add(R.id.frameId,new BlankFragment());
        ft.commit();

    }

}
